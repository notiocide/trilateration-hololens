﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace App.Converters
{
    public class TimeSpanToStringConverter : IValueConverter
    {
        private const ulong Nanosecond = 1;
        private const ulong Microsecond = 1000 * Nanosecond;
        private const ulong Millisecond = 1000 * Microsecond;
        private const ulong Second = 1000 * Millisecond;
        private const ulong Minute = 60 * Second;
        private const ulong Hour = 60 * Minute;
        private const ulong Day = 24 * Hour;
        private const ulong Year = 365 * Day;

        private static readonly (ulong, string)[] Units =
        {
            (Year, "y"),
            (Day, "d"),
            (Hour, "h"),
            (Minute, "m"),
            (Second, "s"),
            (Millisecond, "ms"),
            (Microsecond, "µs"),
            (Nanosecond, "ns")
        };

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var ns = 100 * (ulong) ((TimeSpan) value).Ticks;
            var builder = new StringBuilder();

            var comma = false;

            foreach (var (size, unit) in Units)
            {
                if (ns >= size)
                {
                    if (comma) builder.Append(" ");
                    comma = true;
                    builder.Append(ns / size).Append(" ").Append(unit);
                    ns %= size;
                }
            }

            return builder.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}