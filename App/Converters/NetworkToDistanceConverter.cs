﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.WiFi;
using Windows.UI.Xaml.Data;
using Trilateration;

namespace App.Converters
{
    public class NetworkToDistanceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null) return null;
            var estimator = new FreeSpaceDistanceEstimator();
            var network = (WiFiAvailableNetwork) value;
            return
                $"{estimator.Estimate(network.NetworkRssiInDecibelMilliwatts, (uint) network.ChannelCenterFrequencyInKilohertz)} m";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}