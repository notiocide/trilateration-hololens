﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Devices.WiFi;

using Trilateration;

namespace App.ViewModels
{
    public class MainPageViewModel : NotificationBase
    {
        public ObservableCollection<WiFiAvailableNetwork> Networks { get; } = new ObservableCollection<WiFiAvailableNetwork>();
        
        public ICommand ReloadNetworksCommand { get; }
        public ICommand CalculateCommand { get; }

        private MovementArgs _args = new MovementArgs();

        public async Task Calculate()
        {
            var locator = new XyAngleLocator();

            var data = new MovementArgs();
            await data.Scan(new Vector3(10.45, 23.54));

            IDictionary<AccessPoint, Vector3> points = locator.Locate(data);

            // Do stuff
        }

        public MainPageViewModel()
        {
            ReloadNetworksCommand = new AsyncDelegateCommand(async () =>
            {
                var interfaces = await WiFiAdapter.FindAllAdaptersAsync();
                var seen = new HashSet<string>();

                Networks.Clear();

                foreach (var i in interfaces)
                {
                    await i.ScanAsync();
                    foreach (var n in i.NetworkReport.AvailableNetworks)
                    {
                        if (seen.Contains(n.Bssid)) continue;
                        Networks.Add(n);
                        seen.Add(n.Bssid);
                    }
                }
            });
            CalculateCommand = new AsyncDelegateCommand(Calculate);
        }
    }
}