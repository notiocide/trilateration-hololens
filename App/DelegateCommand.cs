﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace App
{
    public class DelegateCommand : ICommand
    {
        private readonly Action<object> _func;

        public DelegateCommand(Action<object> func) => _func = func;

        public DelegateCommand(Action func) => _func = o => func();

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter) => _func(parameter);

        public event EventHandler CanExecuteChanged;
    }

    public class AsyncDelegateCommand : IAsyncCommand
    {
        private volatile bool _executing = false;
        private readonly Func<object, Task> _func;

        public AsyncDelegateCommand(Func<object, Task> func) => _func = func;

        public AsyncDelegateCommand(Func<Task> func) => _func = o => func();

        public bool CanExecute(object parameter) => !_executing;

        public async Task ExecuteAsync(object parameter)
        {
            if (CanExecute(parameter))
            {
                try
                {
                    _executing = true;
                    await _func(parameter);
                }
                finally
                {
                    _executing = false;
                }
            }

            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        async void ICommand.Execute(object parameter)
        {
            try
            {
                await ExecuteAsync(parameter);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public event EventHandler CanExecuteChanged;
    }
}