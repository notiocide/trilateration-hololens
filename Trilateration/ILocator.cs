﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trilateration
{
    public interface ILocator<in TArgs>
    {
        IDictionary<AccessPoint, Vector3> Locate(TArgs args);
    }
}
