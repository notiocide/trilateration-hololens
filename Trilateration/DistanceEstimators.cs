﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trilateration
{
    /// <summary>
    /// Stolen from https://gist.github.com/eklimcz/446b56c0cb9cfe61d575.
    /// </summary>
    public class ShittyDistanceEstimator : IDistanceEstimator
    {
        public const double TxPower = -59;

        public double Estimate(double rssi, uint frequency)
        {
            var ratio = rssi / TxPower;
            if (ratio < 1.0) return Math.Pow(ratio, 10);
            return 0.89976 * Math.Pow(ratio, 7.7095) + 0.111;
        }
    }

    // FSPL = ((4*pi*d)/lambda)^2
    // d = 10^((rssi+87.55-20log(f))/20)
    public class FreeSpaceDistanceEstimator : IDistanceEstimator
    {
        public double Estimate(double rssi, uint frequency)
        {
            return Math.Pow(10.0, (-rssi + 87.55 - 20 * Math.Log10(frequency)) / 20);
        }
    }
}