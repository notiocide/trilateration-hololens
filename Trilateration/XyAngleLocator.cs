﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.WiFi;

namespace Trilateration
{
    public class XyAngleLocator : IMovementLocator
    {
        private static double EstimateLength(double rssi, uint frequency)
        {
            return new FreeSpaceDistanceEstimator().Estimate(rssi, frequency);
        }

        public IDictionary<AccessPoint, Vector3> Locate(MovementArgs args)
        {
            var map = new Dictionary<AccessPoint, Vector3>();

            foreach (var (ap, measurements) in args)
            {
                var list = measurements.ToList();
                if (list.Count < 2) continue;
                var last = list.First();
                var a = EstimateLength(last.Rssi, last.AccessPoint.Frequency);
                var sum = Vector3.Zero;
                var count = 0u;

                for (var i = 1; i < list.Count; i++)
                {
                    var measurement = list[i];
                    var displacement = measurement.Position - last.Position;
                    var c = displacement.Magnitude;
                    var b = EstimateLength(measurement.Rssi, measurement.AccessPoint.Frequency);
                    var gamma = Math.Acos((c * c - a * a - b * b) / (2 * a * b));

                    var alpha = c / (Math.Sin(gamma) * a);
                    var dir = displacement.Normalize().Rotate2(Math.PI - alpha);
                    var scaledDir = dir * b;

                    var apEstimate = measurement.Position + scaledDir;

                    sum += apEstimate;
                    count++;

                    last = measurement;
                    a = b;
                }

                map[ap] = sum / count;
            }

            return map;
        }
    }
}