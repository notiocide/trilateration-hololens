﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.WiFi;
using Windows.Networking.Connectivity;
using Microsoft.VisualBasic.CompilerServices;

namespace Trilateration
{
    public readonly struct AccessPoint
    {
        public readonly TimeSpan BeaconInterval;
        public readonly string MacAddress;
        public readonly uint Frequency;
        public readonly bool IsWiFiDirect;
        public readonly WiFiNetworkKind Type;
        public readonly WiFiPhyKind PhyType;
        public readonly NetworkAuthenticationType AuthenticationType;
        public readonly NetworkEncryptionType EncryptionType;
        public readonly string Name;

        public AccessPoint(TimeSpan beaconInterval, string macAddress, uint frequency,
            bool isWiFiDirect, WiFiNetworkKind type, WiFiPhyKind phyType,
            NetworkAuthenticationType authenticationType, NetworkEncryptionType encryptionType, string name)
        {
            BeaconInterval = beaconInterval;
            MacAddress = macAddress;
            Frequency = frequency;
            IsWiFiDirect = isWiFiDirect;
            Type = type;
            PhyType = phyType;
            AuthenticationType = authenticationType;
            EncryptionType = encryptionType;
            Name = name;
        }

        public bool Equals(AccessPoint other)
        {
            return BeaconInterval.Equals(other.BeaconInterval) && string.Equals(MacAddress, other.MacAddress) &&
                   Frequency == other.Frequency && IsWiFiDirect == other.IsWiFiDirect &&
                   Type == other.Type && PhyType == other.PhyType &&
                   AuthenticationType == other.AuthenticationType && EncryptionType == other.EncryptionType &&
                   string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is AccessPoint other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = BeaconInterval.GetHashCode();
                hashCode = (hashCode * 397) ^ (MacAddress != null ? MacAddress.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int) Frequency;
                hashCode = (hashCode * 397) ^ IsWiFiDirect.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) Type;
                hashCode = (hashCode * 397) ^ (int) PhyType;
                hashCode = (hashCode * 397) ^ (int) AuthenticationType;
                hashCode = (hashCode * 397) ^ (int) EncryptionType;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static implicit operator AccessPoint(WiFiAvailableNetwork network) => new AccessPoint(
            network.BeaconInterval,
            network.Bssid,
            (uint) network.ChannelCenterFrequencyInKilohertz,
            network.IsWiFiDirect,
            network.NetworkKind,
            network.PhyKind,
            network.SecuritySettings.NetworkAuthenticationType,
            network.SecuritySettings.NetworkEncryptionType,
            network.Ssid
        );
    }
}