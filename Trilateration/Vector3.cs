﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trilateration
{
    public readonly struct Vector3
    {
        public readonly double Coordinate1;
        public readonly double Coordinate2;
        public readonly double Coordinate3;

        public static Vector3 X = new Vector3(1, 0, 0);
        public static Vector3 Y = new Vector3(0, 1, 0);
        public static Vector3 Z = new Vector3(0, 0, 1);
        public static Vector3 Zero = new Vector3(0, 0, 0);

        public Vector3(double coordinate1, double coordinate2) => (Coordinate1, Coordinate2, Coordinate3) = (coordinate1, coordinate2, 0);

        public Vector3(double coordinate1, double coordinate2, double coordinate3) => (Coordinate1, Coordinate2, Coordinate3) = (coordinate1, coordinate2, coordinate3);

        public static Vector3 operator +(Vector3 a, Vector3 b)
        {
            return new Vector3(a.Coordinate1 + b.Coordinate1, a.Coordinate2 + b.Coordinate2,
                a.Coordinate3 + b.Coordinate3);
        }

        public static Vector3 operator -(Vector3 a, Vector3 b)
        {
            return new Vector3(a.Coordinate1 - b.Coordinate1, a.Coordinate2 - b.Coordinate2,
                a.Coordinate3 - b.Coordinate3);
        }

        public static Vector3 operator *(Vector3 vector, double scalar)
        {
            return new Vector3(vector.Coordinate1 * scalar, vector.Coordinate2 * scalar,
                vector.Coordinate3 * scalar);
        }

        public static Vector3 operator *(double scalar, Vector3 vector)
        {
            return new Vector3(vector.Coordinate1 * scalar, vector.Coordinate2 * scalar,
                vector.Coordinate3 * scalar);
        }

        public static Vector3 operator /(Vector3 vector, double scalar)
        {
            return new Vector3(vector.Coordinate1 / scalar, vector.Coordinate2 / scalar,
                vector.Coordinate3 / scalar);
        }

        public double Magnitude => Math.Sqrt(LengthSquared);
        public double LengthSquared => Coordinate1 * Coordinate1 + Coordinate2 * Coordinate2 + Coordinate3 * Coordinate3;


        public Vector3 Normalize()
        {
            var magnitude = Magnitude;
            return new Vector3(Coordinate1 / magnitude, Coordinate2 / magnitude, Coordinate3 / magnitude);
        }

        public Vector3 Rotate2(double angle)
        {
            var sin = Math.Sin(angle);
            var cos = Math.Cos(angle);

            return new Vector3(
                Coordinate1 * cos - Coordinate2 * sin,
                Coordinate1 * sin + Coordinate2 * cos,
                Coordinate3
            );
        }
    }
}