﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.WiFi;

namespace Trilateration
{
    public readonly struct SignalMeasurement
    {
        public readonly Vector3 Position;
        public readonly AccessPoint AccessPoint;
        public readonly double Rssi;

        public SignalMeasurement(Vector3 position, AccessPoint accessPoint, double rssi)
        {
            Position = position;
            AccessPoint = accessPoint;
            Rssi = rssi;
        }
    }

    public class MovementArgs : EventArgs, IEnumerable<(AccessPoint, IEnumerable<SignalMeasurement>)>
    {
        private readonly IDictionary<AccessPoint, IList<SignalMeasurement>> _data;

        public MovementArgs() => _data = new Dictionary<AccessPoint, IList<SignalMeasurement>>();

        public MovementArgs(IDictionary<AccessPoint, IList<SignalMeasurement>> data) =>
            _data = new Dictionary<AccessPoint, IList<SignalMeasurement>>(data);

        public IEnumerator<(AccessPoint, IEnumerable<SignalMeasurement>)> GetEnumerator()
        {
            foreach (var (bssid, data) in _data) yield return (bssid, new ReadOnlyCollection<SignalMeasurement>(data));
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void Add(Vector3 position, IDictionary<AccessPoint, double> strengths)
        {
            foreach (var (ap, rssi) in strengths)
            {
                var measurements = _data.TryGetValue(ap, out var result)
                    ? result
                    : _data[ap] = new List<SignalMeasurement>();

                measurements.Add(new SignalMeasurement(position, ap, rssi));
            }
        }

        public async Task Scan(Vector3 position)
        {
            var interfaces = await WiFiAdapter.FindAllAdaptersAsync();
            var seen = new HashSet<string>();
            
            var strengths = new Dictionary<AccessPoint, double>();

            foreach (var i in interfaces)
            {
                await i.ScanAsync();
                foreach (var n in i.NetworkReport.AvailableNetworks)
                {
                    if (seen.Contains(n.Bssid)) continue;

                    strengths.Add(n, n.NetworkRssiInDecibelMilliwatts);

                    seen.Add(n.Bssid);
                }
            }

            Add(position, strengths);
        }
    }
}