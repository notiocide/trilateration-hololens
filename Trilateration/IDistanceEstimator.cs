﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trilateration
{
    public interface IDistanceEstimator
    {
        double Estimate(double rssi, uint frequency);
    }
}
